package barbero;

/**
 * Clase que representa a un cliente.
 * Esta clase por si sola, no hace nada y solo sirve para mostrar mensajes e interactuar con los
 * otros objetos
 */
public class Cliente {

	/**
	 * Mensaje de llegada del cliente
	 */
	public void llegando() {
		System.out.println("Un cliente nuevo llega.");
	}

	/**
	 * Mensaje de salida del cliente
	 *
	 * @param atendido esta opción quiere decir si el cliente ha sido atendido o no
	 */
	public void irse(boolean atendido) {
		// Si el cliente es atendido, entonces el cliente se va satisfecho.
		// En programación poner solo la variable es el equivalente a poner:
		// if (atendio == true)
		// pero realmente no es necesario, ya que automáticamente es inferido
		if (atendido) {
			System.out.println("El cliente se fue contento con su nuevo estilo.");
		} else {
			// Si el cliente no fue atendido, quiere decir que no había lugares y tiene que irse
			System.out.println("El cliente se fue, porque no abia más lugares.");
		}
	}

}
