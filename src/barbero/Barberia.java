package barbero;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Clase de tipo [Barberia].
 * Esta clase sirve como contenedor de los objetos cliente.
 * <p>
 * Esta clase extiende de {@link Thread} para que no bloquee el proceso de otros objetos
 */
public class Barberia extends Thread {

	/**
	 * Máximo tiempo que un cliente tarda en llegar
	 */
	private static final long MAXIMO_TIEMPO_CLIENTE_LLEGA = 10_000L;

	/**
	 * Número total de sillas de espera que tiene la barbería
	 */
	private static final int TOTAL_SILLAS = 3;

	/**
	 * Objeto random para acciones aleatorias
	 * en la barbería.
	 */
	public static final Random random = new Random(System.currentTimeMillis());

	/**
	 * Barbero que atiende la barbería
	 */
	private final Barbero barbero;

	/**
	 * Colección con todos los clientes.
	 * <p>
	 * Esta lista pertenece a los objetos sincronizados,
	 * lo cual nos evita tener que hacerlo manualmente.
	 */
	private final List<Cliente> clientesEsperando = Collections
		.synchronizedList(new ArrayList<>());

	/**
	 * Constructor por defecto
	 */
	public Barberia() {
		super();
		barbero = new Barbero(this);
	}

	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */

	/**
	 * Ciclo de vida de la barberia
	 */
	@SuppressWarnings({"InfiniteLoopStatement", "BusyWait"})
	@Override
	public void run() {
		// Iniciamos el hilo del barbero
		barbero.start();
		// Realizamos un bucle infinito
		while (true) {
			// Calcular periodo de tiempo en segundos.
			// Este tiempo es utilizado para simular la llegada de un nuevo cliente,
			// de esta forma obtenemos un comportamiento más aleatorio
			long siguienteTiempo = (long) Math.floor(random.nextFloat() * MAXIMO_TIEMPO_CLIENTE_LLEGA);

			try {
				// Creamos un nuevo cliente
				Cliente cliente = new Cliente();
				cliente.llegando();

				// Solo verificar si el barbero está dormido. Eso significa que eres el primer cliente en llegar
				if (barbero.getEstado() == Barbero.Estado.DORMIDO)
					barbero.despertar();

				// Agregamos el cliente a la cola
				agregarClienteACola(cliente);

				// Dormimos el hilo hasta la llegada de otro nuevo cliente
				Thread.sleep(siguienteTiempo);
			} catch (Exception ignore) {
			}
		}
	}

	/**
	 * Insertamos el cliente a la cola de espera
	 *
	 * @param cliente el cliente que se agregará
	 */
	public void agregarClienteACola(Cliente cliente) {
		// Verificamos si el cliente alcanza lugar en la barbería
		if (clientesEsperando.size() > TOTAL_SILLAS) {
			// El cliente se va porque no hay lugares
			cliente.irse(false);
			return;
		}

		// Agregamos al nuevo cliente a la cola
		clientesEsperando.add(cliente);
	}

	/**
	 * Pasamos al siguiente cliente de la cola
	 *
	 * @return el siguiente cliente
	 */
	public Cliente siguienteCliente() {
		// Verificamos si la espera esta vacía
		if (clientesEsperando.isEmpty()) return null;

		// Devolvemos el primer cliente (en términos de tiempo es el que llego antes que los demás)
		Cliente siguiente = clientesEsperando.get(0);
		// También lo eliminamos de la lista para que no ocupe el lugar.
		// Recordemos que tanto las listas como las matrices su índice se empieza a contar
		// desde 0
		clientesEsperando.remove(0);

		return siguiente;
	}

}
