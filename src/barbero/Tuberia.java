package barbero;

public class Tuberia {

	/**
	 * Método principal del programa
	 *
	 * @param args argumentos del programa
	 * @throws Exception error si el programa es interrumpido
	 */
	public static void main(String[] args) throws Exception {
		// Iniciamos una instancia de la clase barbería
		Barberia barberia = new Barberia();

		// Iniciamos el hilo de ejecución y esperamos
		// hasta que se termine su ejecución
		barberia.start();
		barberia.join();
	}

}
